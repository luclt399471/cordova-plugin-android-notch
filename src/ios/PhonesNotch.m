/********* NotchPlugin.m Cordova Plugin Implementation *******/

#import <Cordova/CDVPlugin.h>

@interface PhonesNotch : CDVPlugin {
  // Member variables go here.
}

- (void)hasCutout:(CDVInvokedUrlCommand*)command;
- (void)getInsetsTop:(CDVInvokedUrlCommand*)command;
@end

@implementation PhonesNotch

- (void)hasCutout:(CDVInvokedUrlCommand*)command
{
  CDVPluginResult* pluginResult = nil;

  BOOL hasNotch;
  hasNotch = NO;

  if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        hasNotch = window.safeAreaInsets.bottom > 0;
  }

  pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:hasNotch];
  [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getInsetsTop:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;

    double topPadding = 0;

    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
    }

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDouble:topPadding];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end