

var exec = require("cordova/exec");

var PhonesNotch = {

    hasCutout: function(success, fail) {
        exec(success, fail, "PhonesNotch", "hasCutout", []);
    },

    setLayout: function(success, fail) {
        exec(success, fail, "PhonesNotch", "setLayout", []);
    },

    getInsetTop: function (success, fail) {
        exec(success, fail, "PhonesNotch", "getInsetsTop", []);
    },

    getInsetRight: function (success, fail) {
        exec(success, fail, "PhonesNotch", "getInsetsRight", []);
    },

    getInsetBottom: function (success, fail) {
        exec(success, fail, "PhonesNotch", "getInsetsBottom", []);
    },

    getInsetLeft: function (success, fail) {
        exec(success, fail, "PhonesNotch", "getInsetsLeft", []);
    }
};

module.exports = PhonesNotch;
